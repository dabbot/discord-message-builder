use std::{
    default::Default,
    fmt::{self, Display, Write},
    ops::Add,
};

#[derive(Clone, Debug, Default)]
pub struct MessageBuilder(pub String);

impl MessageBuilder {
    pub fn new() -> MessageBuilder { MessageBuilder::default() }

    pub fn build(self) -> String { self.0 }

    pub fn channel(mut self, channel_id: u64) -> Self {
        let _ = write!(self.0, "<#{}>", channel_id);

        self
    }

    #[inline]
    pub fn push<D: I>(self, content: D) -> Self {
        self._push(&content.into().to_string())
    }

    fn _push(mut self, content: &str) -> Self {
        self.0.push_str(content);

        self
    }

    pub fn push_codeblock<D: I>(mut self, content: D, language: Option<&str>) -> Self {
        self.0.push_str("```");

        if let Some(language) = language {
            self.0.push_str(language);
        }

        self.0.push('\n');
        self.0.push_str(&content.into().to_string());
        self.0.push_str("\n```");

        self
    }

    pub fn push_mono<D: I>(mut self, content: D) -> Self {
        self.0.push('`');
        self.0.push_str(&content.into().to_string());
        self.0.push('`');

        self
    }

    pub fn push_italic<D: I>(mut self, content: D) -> Self {
        self.0.push('_');
        self.0.push_str(&content.into().to_string());
        self.0.push('_');

        self
    }

    pub fn push_bold<D: I>(mut self, content: D) -> Self {
        self.0.push_str("**");
        self.0.push_str(&content.into().to_string());
        self.0.push_str("**");

        self
    }

    pub fn push_underline<D: I>(mut self, content: D) -> Self {
        self.0.push_str("__");
        self.0.push_str(&content.into().to_string());
        self.0.push_str("__");

        self
    }

    pub fn push_strike<D: I>(mut self, content: D) -> Self {
        self.0.push_str("~~");
        self.0.push_str(&content.into().to_string());
        self.0.push_str("~~");

        self
    }

    pub fn push_line<D: I>(mut self, content: D) -> Self {
        self = self.push(content);
        self.0.push('\n');

        self
    }

    pub fn push_mono_line<D: I>(mut self, content: D) -> Self {
        self = self.push_mono(content);
        self.0.push('\n');

        self
    }

    pub fn push_italic_line<D: I>(mut self, content: D) -> Self {
        self = self.push_italic(content);
        self.0.push('\n');

        self
    }

    pub fn push_bold_line<D: I>(mut self, content: D) -> Self {
        self = self.push_bold(content);
        self.0.push('\n');

        self
    }

    pub fn push_underline_line<D: I>(mut self, content: D) -> Self {
        self = self.push_underline(content);
        self.0.push('\n');

        self
    }

    pub fn push_strike_line<D: I>(mut self, content: D) -> Self {
        self = self.push_strike(content);
        self.0.push('\n');

        self
    }

    pub fn push_safe<C: I>(mut self, content: C) -> Self {
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner)
                .replace('*', "\\*")
                .replace('`', "\\`")
                .replace('_', "\\_");

            self.0.push_str(&c.to_string());
        }

        self
    }

    pub fn push_codeblock_safe<D: I>(mut self, content: D, language: Option<&str>) -> Self {
        self.0.push_str("```");

        if let Some(language) = language {
            self.0.push_str(language);
        }

        self.0.push('\n');

        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace("```", " ");
            self.0.push_str(&c.to_string());
        }
        self.0.push_str("```");

        self
    }

    pub fn push_mono_safe<D: I>(mut self, content: D) -> Self {
        self.0.push('`');
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace('`', "'");
            self.0.push_str(&c.to_string());
        }
        self.0.push('`');

        self
    }

    pub fn push_italic_safe<D: I>(mut self, content: D) -> Self {
        self.0.push('_');
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace('_', " ");
            self.0.push_str(&c.to_string());
        }
        self.0.push('_');

        self
    }

    pub fn push_bold_safe<D: I>(mut self, content: D) -> Self {
        self.0.push_str("**");
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace("**", " ");
            self.0.push_str(&c.to_string());
        }
        self.0.push_str("**");

        self
    }

    pub fn push_underline_safe<D: I>(mut self, content: D) -> Self {
        self.0.push_str("__");
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace("__", " ");
            self.0.push_str(&c.to_string());
        }
        self.0.push_str("__");

        self
    }

    pub fn push_strike_safe<D: I>(mut self, content: D) -> Self {
        self.0.push_str("~~");
        {
            let mut c = content.into();
            c.inner = normalize(&c.inner).replace("~~", " ");
            self.0.push_str(&c.to_string());
        }
        self.0.push_str("~~");

        self
    }

    pub fn push_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_safe(content);
        self.0.push('\n');

        self
    }

    pub fn push_mono_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_mono_safe(content);
        self.0.push('\n');

        self
    }

    pub fn push_italic_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_italic_safe(content);
        self.0.push('\n');

        self
    }

    pub fn push_bold_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_bold_safe(content);
        self.0.push('\n');

        self
    }

    pub fn push_underline_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_underline_safe(content);
        self.0.push('\n');

        self
    }

    pub fn push_strike_line_safe<D: I>(mut self, content: D) -> Self {
        self = self.push_strike_safe(content);
        self.0.push('\n');

        self
    }

    pub fn role(mut self, role_id: u64) -> Self {
        let _ = write!(self.0, "<@&{}>", role_id);

        self
    }

    pub fn user(mut self, user_id: u64) -> Self {
        let _ = write!(self.0, "<@{}>", user_id);

        self
    }
}

impl Display for MessageBuilder {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { fmt::Display::fmt(&self.0, f) }
}

pub enum ContentModifier {
    Italic,
    Bold,
    Strikethrough,
    Code,
    Underline,
}

/// Describes formatting on string content
#[derive(Debug, Default, Clone)]
pub struct Content {
    pub italic: bool,
    pub bold: bool,
    pub strikethrough: bool,
    pub inner: String,
    pub code: bool,
    pub underline: bool,
}

impl<T: ToString> Add<T> for Content {
    type Output = Content;

    fn add(mut self, rhs: T) -> Content {
        self.inner = self.inner + &rhs.to_string();

        self
    }
}

impl<T: ToString> Add<T> for ContentModifier {
    type Output = Content;

    fn add(self, rhs: T) -> Content {
        let mut nc = self.to_content();
        nc.inner = nc.inner + &rhs.to_string();

        nc
    }
}

impl Add<ContentModifier> for Content {
    type Output = Content;

    fn add(mut self, rhs: ContentModifier) -> Content {
        self.apply(&rhs);

        self
    }
}

impl Add<ContentModifier> for ContentModifier {
    type Output = Content;

    fn add(self, rhs: ContentModifier) -> Content {
        let mut nc = self.to_content();
        nc.apply(&rhs);

        nc
    }
}

impl ContentModifier {
    fn to_content(&self) -> Content {
        let mut nc = Content::default();
        nc.apply(self);

        nc
    }
}

impl Content {
    pub fn apply(&mut self, modifier: &ContentModifier) {
        match *modifier {
            ContentModifier::Italic => {
                self.italic = true;
            },
            ContentModifier::Bold => {
                self.bold = true;
            },
            ContentModifier::Strikethrough => {
                self.strikethrough = true;
            },
            ContentModifier::Code => {
                self.code = true;
            },
            ContentModifier::Underline => {
                self.underline = true;
            },
        }
    }

    pub fn to_string(&self) -> String {
        let capacity =
            self.inner.len() + if self.bold { 4 } else { 0 } + if self.italic { 2 } else { 0 } +
            if self.strikethrough { 4 } else { 0 } + if self.underline {
                4
            } else {
                0
            } + if self.code { 2 } else { 0 };
        let mut new_str = String::with_capacity(capacity);

        if self.bold {
            new_str.push_str("**");
        }

        if self.italic {
            new_str.push('*');
        }

        if self.strikethrough {
            new_str.push_str("~~");
        }

        if self.underline {
            new_str.push_str("__");
        }

        if self.code {
            new_str.push('`');
        }

        new_str.push_str(&self.inner);

        if self.code {
            new_str.push('`');
        }

        if self.underline {
            new_str.push_str("__");
        }

        if self.strikethrough {
            new_str.push_str("~~");
        }

        if self.italic {
            new_str.push('*');
        }

        if self.bold {
            new_str.push_str("**");
        }

        new_str
    }
}

impl From<ContentModifier> for Content {
    fn from(cm: ContentModifier) -> Content { cm.to_content() }
}

mod private {
    use super::{Content, ContentModifier};
    use std::fmt;

    pub trait A {}

    impl A for ContentModifier {}
    impl A for Content {}
    impl<T: fmt::Display> A for T {}
}


/// This trait only exists as way to bypass the shouting of the compiler. Specifically "conflicting
/// implementations in core" and alike.
/// However is not meant to be used outside.
pub trait I: self::private::A {
    fn into(self) -> Content;
}

impl<T: fmt::Display> I for T {
    fn into(self) -> Content {
        Content {
            italic: false,
            bold: false,
            strikethrough: false,
            inner: self.to_string(),
            code: false,
            underline: false,
        }
    }
}

impl I for ContentModifier {
    fn into(self) -> Content { self.to_content() }
}

impl I for Content {
    fn into(self) -> Content { self }
}

fn normalize(text: &str) -> String {
    // Remove invite links and popular scam websites, mostly to prevent the
    // current user from triggering various ad detectors and prevent embeds.
    text.replace("discord.gg", "discord\u{2024}gg")
        .replace("discord.me", "discord\u{2024}me")
        .replace("discordlist.net", "discordlist\u{2024}net")
        .replace("discordservers.com", "discordservers\u{2024}com")
        .replace("discordapp.com/invite", "discordapp\u{2024}com/invite")
        // Remove right-to-left override and other similar annoying symbols
        .replace('\u{202E}', " ") // RTL Override
        .replace('\u{200F}', " ") // RTL Mark
        .replace('\u{202B}', " ") // RTL Embedding
        .replace('\u{200B}', " ") // Zero-width space
        .replace('\u{200D}', " ") // Zero-width joiner
        .replace('\u{200C}', " ") // Zero-width non-joiner
        // Remove everyone and here mentions. Has to be put after ZWS replacement
        // because it utilises it itself.
        .replace("@everyone", "@\u{200B}everyone")
        .replace("@here", "@\u{200B}here")
}

#[cfg(test)]
mod test {
    use super::{
        ContentModifier::*,
        MessageBuilder,
    };

    macro_rules! gen {
        ($($fn:ident => [$($text:expr => $expected:expr),+]),+) => ({
            $(
                $(
                    assert_eq!(MessageBuilder::new().$fn($text).0, $expected);
                )+
            )+
        });
    }

    #[test]
    fn code_blocks() {
        let content = MessageBuilder::new()
            .push_codeblock("test", Some("rb"))
            .build();
        assert_eq!(content, "```rb\ntest\n```");
    }

    #[test]
    fn safe_content() {
        let content = MessageBuilder::new()
            .push_safe("@everyone discord.gg/discord-api")
            .build();
        assert_ne!(content, "@everyone discord.gg/discord-api");
    }

    #[test]
    fn no_free_formatting() {
        let content = MessageBuilder::new().push_bold_safe("test**test").build();
        assert_ne!(content, "**test**test**");
    }

    #[test]
    fn mentions() {
        let content_mentions = MessageBuilder::new()
            .channel(1)
            .role(3)
            .user(4)
            .build();
        assert_eq!(content_mentions, "<#1><@&3><@4>");
    }

    #[test]
    fn content() {
        let content = Bold + Italic + Code + "Fun!";

        assert_eq!(content.to_string(), "***`Fun!`***");
    }

    #[test]
    fn init() {
        assert_eq!(MessageBuilder::new().0, "");
        assert_eq!(MessageBuilder::default().0, "");
    }

    #[test]
    fn message_content() {
        let message_content = MessageBuilder::new()
            .push(Bold + Italic + Code + "Fun!")
            .build();

        assert_eq!(message_content, "***`Fun!`***");
    }

    #[test]
    fn message_content_safe() {
        let message_content = MessageBuilder::new()
            .push_safe(Bold + Italic + "test**test")
            .build();

        assert_eq!(message_content, "***test\\*\\*test***");
    }

    #[test]
    fn push() {
        assert_eq!(MessageBuilder::new().push('a').0, "a");
        assert!(MessageBuilder::new().push("").0.is_empty());
    }

    #[test]
    fn push_codeblock() {
        let content = MessageBuilder::new().push_codeblock("foo", None).0;
        assert_eq!(content, "```\nfoo\n```");

        let content = MessageBuilder::new()
            .push_codeblock("fn main() { }", Some("rs"))
            .0;
        assert_eq!(content, "```rs\nfn main() { }\n```");
    }

    #[test]
    fn push_codeblock_safe() {
        assert_eq!(
            MessageBuilder::new().push_codeblock_safe("foo", Some("rs")).0,
            "```rs\nfoo```",
        );
        assert_eq!(
            MessageBuilder::new().push_codeblock_safe("", None).0,
            "```\n```",
        );
        assert_eq!(
            MessageBuilder::new().push_codeblock_safe("1 * 2", None).0,
            "```\n1 * 2```",
        );
        assert_eq!(
            MessageBuilder::new().push_codeblock_safe("`1 * 3`", None).0,
            "```\n`1 * 3````",
        );
        assert_eq!(
            MessageBuilder::new().push_codeblock_safe("```.```", None).0,
            "```\n . ```",
        );
    }

    #[test]
    fn push_safe() {
        gen! {
            push_safe => [
                "" => "",
                "foo" => "foo",
                "1 * 2" => "1 \\* 2"
            ],
            push_bold_safe => [
                "" => "****",
                "foo" => "**foo**",
                "*foo*" => "***foo***",
                "f*o**o" => "**f*o o**"
            ],
            push_italic_safe => [
                "" => "__",
                "foo" => "_foo_",
                "f_o_o" => "_f o o_"
            ],
            push_mono_safe => [
                "" => "``",
                "foo" => "`foo`",
                "asterisk *" => "`asterisk *`",
                "`ticks`" => "`'ticks'`"
            ],
            push_strike_safe => [
                "" => "~~~~",
                "foo" => "~~foo~~",
                "foo ~" => "~~foo ~~~",
                "~~foo" => "~~ foo~~",
                "~~fo~~o~~" => "~~ fo o ~~"
            ],
            push_underline_safe => [
                "" => "____",
                "foo" => "__foo__",
                "foo _" => "__foo ___",
                "__foo__ bar" => "__ foo  bar__"
            ],
            push_line_safe => [
                "" => "\n",
                "foo" => "foo\n",
                "1 * 2" => "1 \\* 2\n"
            ],
            push_mono_line_safe => [
                "" => "``\n",
                "a ` b `" => "`a ' b '`\n"
            ],
            push_italic_line_safe => [
                "" => "__\n",
                "a * c" => "_a * c_\n"
            ],
            push_bold_line_safe => [
                "" => "****\n",
                "a ** d" => "**a   d**\n"
            ],
            push_underline_line_safe => [
                "" => "____\n",
                "a __ e" => "__a   e__\n"
            ],
            push_strike_line_safe => [
                "" => "~~~~\n",
                "a ~~ f" => "~~a   f~~\n"
            ]
        };
    }

    #[test]
    fn push_unsafe() {
        gen! {
            push_bold => [
                "a" => "**a**",
                "" => "****",
                '*' => "*****",
                "**" => "******"
            ],
            push_bold_line => [
                "" => "****\n",
                "foo" => "**foo**\n"
            ],
            push_italic => [
                "a" => "_a_",
                "" => "__",
                "_" => "___",
                "__" => "____"
            ],
            push_italic_line => [
                "" => "__\n",
                "foo" => "_foo_\n",
                "_?" => "__?_\n"
            ],
            push_line => [
                "" => "\n",
                "foo" => "foo\n",
                "\n\n" => "\n\n\n",
                "\nfoo\n" => "\nfoo\n\n"
            ],
            push_mono => [
                "a" => "`a`",
                "" => "``",
                "`" => "```",
                "``" => "````"
            ],
            push_mono_line => [
                "" => "``\n",
                "foo" => "`foo`\n",
                "\n" => "`\n`\n",
                "`\n`\n" => "``\n`\n`\n"
            ],
            push_strike => [
                "a" => "~~a~~",
                "" => "~~~~",
                "~" => "~~~~~",
                "~~" => "~~~~~~"
            ],
            push_strike_line => [
                "" => "~~~~\n",
                "foo" => "~~foo~~\n"
            ],
            push_underline => [
                "a" => "__a__",
                "" => "____",
                "_" => "_____",
                "__" => "______"
            ],
            push_underline_line => [
                "" => "____\n",
                "foo" => "__foo__\n"
            ]
        };
    }

    #[test]
    fn normalize() {
        assert_eq!(super::normalize("@everyone"), "@\u{200B}everyone");
        assert_eq!(super::normalize("@here"), "@\u{200B}here");
        assert_eq!(super::normalize("discord.gg"), "discord\u{2024}gg");
        assert_eq!(super::normalize("discord.me"), "discord\u{2024}me");
        assert_eq!(super::normalize("discordlist.net"), "discordlist\u{2024}net");
        assert_eq!(super::normalize("discordservers.com"), "discordservers\u{2024}com");
        assert_eq!(super::normalize("discordapp.com/invite"), "discordapp\u{2024}com/invite");
        assert_eq!(super::normalize("\u{202E}"), " ");
        assert_eq!(super::normalize("\u{200F}"), " ");
        assert_eq!(super::normalize("\u{202B}"), " ");
        assert_eq!(super::normalize("\u{200B}"), " ");
        assert_eq!(super::normalize("\u{200D}"), " ");
        assert_eq!(super::normalize("\u{200C}"), " ");
    }
}
