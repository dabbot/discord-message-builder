# discord-message-builder

It's like [serenity]'s message builder, but without the "serenity" part of it.

This is a 0-dependency escaped message builder.

[Docs here].

[Docs here]: https://docs.rs/serenity/*/serenity/utils/struct.MessageBuilder.html
[serenity]: https://github.com/serenity-rs/serenity
